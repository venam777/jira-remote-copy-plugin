package com.atlassian.cpji.conditions;

import com.atlassian.cpji.config.IssueCategories;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractIssueWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 *
 */
public class CanMoveToThirdLineCondition extends AbstractIssueWebCondition {

    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, Issue issue, JiraHelper jiraHelper) {
        return issue.getIssueType() != null && issue.getIssueType().getName().equalsIgnoreCase(IssueCategories.REQUEST_FOR_MODIFICATION.getCaption());
    }
}
